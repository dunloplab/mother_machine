"""
This script, when run through KLayout's Macro development interface, generates
a GDSII file for a mask with 6 mother machine devices, and each machine has 8
main flow channels with 3 sets of 1000 mother machine chambers.

This design is meant to be printed on two different masks. To get GDS files for
each layer, comment one of the mask.insert(final) lines at the end of this 
script.

@author: jblugagne
"""

import pya # KLayout Python API. See here: https://www.klayout.de/doc-qt5/code/index.html
from  math import sqrt

#%% Function definitions
def makecircle( center_x, center_y, radius, vertices=40):
    '''
    "Render" a disk. This will return a polygon approximating a disk.

    Parameters
    ----------
    center_x : int or float
        X coordinate of the disk center.
    center_y : int or float
        Y coordinate of the disk center.
    radius : int or float
        Disk radius.
    vertices : int, optional
        Number of vertices, or edges, for the approximating polygon.
        The default is 40.

    Returns
    -------
    circle : pya.SimplePolygon
        A polygon shape approximating a disk.

    '''
    
    circle = pya.Box(pya.Point(center_x-radius,center_y-radius),pya.Point(center_x+radius,center_y+radius))
    circle = pya.SimplePolygon(circle)
    circle = circle.round_corners(radius,radius,vertices)
    return circle
    
def makechamber( x, y, width=1.5e3, length=25e3,overlap=5e3,funnel=True, roundends=True,funnel_radius=10e3,funnel_depth=5e3):
    '''
    Build a mother machine chamber

    Parameters
    ----------
    x : int or float
        X coordinate of the open end of the chamber, ie where it is supposed
        to connect with the main/flow channel.
    y : TYPE
        Y coordinate of the open end of the chamber, ie where it is supposed
        to connect with the main/flow channel.
    width : int or float, optional
        Width of the chamber. 
        The default is 1.5e3.
    length : int or float, optional
        Length of the chamber. 
        The default is 25e3.
    overlap : int or float, optional
        The length of extension of the chamber after the open end, ie how much
        overlap there will be between the chambers and the flow channels. 
        The default is 25e3.
    funnel : bool, optional
        If True, a funnel will be built at the open end of the chamber.
        The default is False.
    roundends : bool, optional
        If True, the closed end of the chamber will be rounded. 
        The default is True.
    funnel_radius : int or float, optional
        Radius describing the funnel's curvature.
        The default is 10e3.
    funnel_depth : int or float, optional
        Length of chamber with the funnel (not counting overlap part). 
        The default is 5e3.

    Returns
    -------
    chamber : pya.Region
        Mother machine chamber.

    '''
    
    # Generate base rectangle:
    chamber = pya.Box(pya.Point(x-width/2,y-length),pya.Point(x+width/2,y+overlap))
    chamber = pya.SimplePolygon(chamber)
    # Round the chamber ends if roundends == True
    if roundends:
        chamber = chamber.round_corners(width,width,30)
    # Cast into Region for boolean operations:
    chamber = pya.Region(chamber)
    if funnel:
        # Generate funnel by creating a square and removing disks portions on the side:
        funnel = pya.Box(pya.Point(x-(width/2+funnel_radius),y-funnel_depth),pya.Point(x+(width/2+funnel_radius),y+overlap))
        disk = makecircle(x-(width/2+funnel_radius),y-funnel_depth, funnel_radius, 200)
        funnel = pya.Region(funnel)
        disk = pya.Region(disk)
        funnel -= disk
        disk = makecircle(x+(width/2+funnel_radius),y-funnel_depth, funnel_radius, 200)
        disk = pya.Region(disk)
        funnel -= disk
        # Add funnel to chamber:
        chamber += funnel
        # Fuse shapes together:
        chamber = chamber.merge()
    return chamber
    
def writetext(text, layout, size):
    '''
    Write text as a cell

    Parameters
    ----------
    text : str
        Text to write.
    layout : pya.layout
        Current layout.
    size : int or float
        Vertical size of text.

    Returns
    -------
    tempcell: pya.Cell
        Cell containing the text

    '''    
    
    blib = pya.Library.library_by_name("Basic")
    pid = blib.layout().pcell_declaration("TEXT")
    tempcell = layout.create_cell(text)
    param = { 
      "text": text, 
      "layer": pya.LayerInfo(1, 0),  # target layer of the text: layer 17, datatype 5 
      "mag": size/700
    }
    pv = []
    for p in pid.get_parameters():
      if p.name in param:
        pv.append(param[p.name])
      else:
        pv.append(p.default)
    
    # create the PCell variant cell
    pcell_var = layout.add_pcell_variant(blib, pid.id(), pv)
    pcell_inst = tempcell.insert(pya.CellInstArray(pcell_var, pya.Trans()))
    return tempcell

def buildchannel( inlet_y, obs_y, obs_length, bend_radius=2e6, chan_width = 400e3, chan_length=None, inlet_x=None, inlet_radius=500e3):
    '''
    Creates main flow channels. Note that the channels are supposed to be 
    horizontally symmetrical, hence why a reduced number of coordinates is 
    given

    Parameters
    ----------
    inlet_y : int or float
        Y coordinate of the inlets.
    obs_y : int or float
        Y coordinate of the "observation area", ie where the chamber are going 
        to be.
    obs_length : int or float
        Length of the "observation area".
    bend_radius : int, float, or None optional
        Bend/curvature radius of the channels turns/elbows. If None, no 
        curvature is applied.
        The default is 2e6.
    chan_width : int or float, optional
        Width of the channels. 
        The default is 400e3.
    chan_length : int, float, or None, optional
        Length or the channel. If not None and inlet_x is None, the X 
        coordinate of the inlet will be calculated such that the total channel
        length is roughly chan_length
        The default is None.
    inlet_x : int, float, or None, optional
        If not None, sets the inlet X coordinate. Note that, if None, 
        chan_length must be defined. This is the left-side inlet x and must be
        negative.
        The default is None.
    inlet_radius : int, float, or None, optional
        Radius of the inlet disk. If None, no disk is added. 
        The default is 500e3.

    Returns
    -------
    chan : pya.Region
        Flow channel region.

    '''
    
    if inlet_x is None:
        arm_length = (chan_length-obs_length)/2
        #print((arm_length)**2 - (inlet_y - obs_y)**2)
        inlet_x = -(sqrt((arm_length)**2 - (inlet_y - obs_y)**2) + obs_length/2)
    # Start
    currpoint = pya.Point(inlet_x, inlet_y)
    points = [currpoint]
    # First elbow
    currpoint = pya.Point(-obs_length/2, obs_y)
    points.append(currpoint)
    #Second elbow
    currpoint = pya.Point(obs_length/2, obs_y)
    points.append(currpoint)
    #End
    currpoint = pya.Point(-inlet_x, inlet_y)
    points.append(currpoint)
    
    # Create, bend corners, transform into polygon:
    chan = pya.Path(points, chan_width)
    if bend_radius is not None:
        chan = chan.round_corners(bend_radius,100)
    chan = chan.simple_polygon()
    
    chan = pya.Region(chan)
    # Add the disks for the inlets:
    if inlet_radius is not None:
        inlet = makecircle( inlet_x, inlet_y, inlet_radius)
        outlet = makecircle( -inlet_x, inlet_y, inlet_radius)
        chan.insert(inlet)
        chan.insert(outlet)
    chan = chan.merge()
    
    return chan
    

def getinletspositions(num_channels, obs_spacing, inlets_spacing, chan_length, obs_length, y_min = 1e6):
    '''
    Computes inlets positions such that (1) the distance between all inlets is
    constant and == inlets_spacing and (2) the length of all channels is 
    roughly the same. The math is based on the intersection between 2 circles:
    One circle is arm length and the other is inlets spacing. See here:
    http://paulbourke.net/geometry/circlesphere/

    Parameters
    ----------
    num_channels : int
        Number of flow channels.
    obs_spacing : int or float
        Constant distance between all channels in the "observation area".
    inlets_spacing : int or float
        Constant distance between inlets.
    chan_length : int or float
        Total channel length.
    obs_length : int or float
        Length of the observation area.
    y_min : int or float, optional
        Minimum y coordinate for the first inlet. 
        The default is 1e6.

    Returns
    -------
    inlet : list
        A list of 2-element lists containing the x and y coordinates for each
        inlet (only right-side inlets).

    '''
    
    arm_length = (chan_length-obs_length)/2
    # First inlet position:
    inlet = [[sqrt((arm_length)**2 - y_min**2),y_min]]
    for indChan in range(1,num_channels):
        d = sqrt((inlet[indChan-1][1] - indChan*obs_spacing)**2 + (inlet[indChan-1][0])**2)
        a = (arm_length**2 - inlets_spacing**2 + d**2 )/(2*d)
        h = sqrt(arm_length**2 - a**2)
        xi = a*inlet[indChan-1][0]/d - h*(inlet[indChan-1][1]-indChan*obs_spacing)/d
        yi = indChan*obs_spacing + a*(inlet[indChan-1][1]-indChan*obs_spacing)/d + h*inlet[indChan-1][0]/d
        inlet.append([xi,yi])
    # Add obs_length/2 to all inlet positions:
    for indChan in range(num_channels):
        inlet[indChan][0] += obs_length/2
        
    return inlet

#%% PARAMETERS
# distance units are in nm

## Path to git repo:
git_repo_path = "/home/jeanbaptiste/bu/wafers/mother_machine/"

# main channels
num_channels = 8 # Number of channels per chip
max_width = 12e6 # Roughly the width of 1 chip
max_length =  36e6 # Roughly the length of 1 chip
channels_length = max_length # Roughly length of 1 channel (all channels will be approx. the same length)
obs_spacing = 530e3 # Spacing between main channels
inlets_spacing = max_width/(num_channels-1) # Spacing between the tubing inlets (along y axis)
chan_width = 400e3 # Width of the  channels
bend_radius = 2e6 # Bend radius of the turns/elbows of the channels

# chambers
spacing_chambers = 5e3 # Spacing between chambers
width1 =  1300 # Chamber width for set 1 
width2 = 1500 # Chamber width for set 2
width3 = 1800 # Chamber width for set 3
length1 = 25e3 # Chamber length for side 1
num_chambers = 1000 # Number of chambers per set


# Focusing crosses:
cross_size = 15e3
cross_thickness = 2e3
spacing_crosses = 20e3
num_crosses = (3*(num_chambers+3))*spacing_chambers/spacing_crosses
crosses_shift = length1 +  spacing_chambers + cross_size/2

# Text
numberssize = 15e3
bignumberssize = 2e6
expl_text = "ch1l25w1.3"

# General layout:
spacing_machines = max_width + 4.5e6 # Spacing between the different machines that will be on the wafer
num_machines = 4 # Number of machines to put on the chip
align_cross_size = 5e6 # Size of the alignment crosses (This is just informational, changing this value does not change the size of the actual crosses)
aligncross_dist = max_length/2 + align_cross_size + 5e6 # Alignment crosses positions


#%% Build

### Init
layout = pya.Layout()
l1 = layout.layer(1, 0)




### Alignment crosses:
# Instantiate chambers and channels with alignment crosses:
#Load alignment cross from file: (Thank you Clément for the crosses)
layout.read(git_repo_path + "alignment_crosses/maincross.gds")
layout.move_layer(1,0) # Not sure why it keeps putting them on new different layers
layout.clear_layer(1)
alignment_cross = layout.cell("alignment_cross")
alignment_crosses = pya.CellInstArray(alignment_cross.cell_index(),
                                      pya.Trans(pya.Vector(-aligncross_dist,-aligncross_dist)),
                                      pya.Vector(aligncross_dist*2,0),
                                      pya.Vector(0,aligncross_dist*2),
                                      2,
                                      2)
align_crosses = layout.create_cell("alignment_crosses")
align_crosses.insert(alignment_crosses)





### Mother Machine Chambers:
# Create chamber with 1.3um width:
chamber_1_25e3 = layout.create_cell("chamber_1_25e3")
chamber = makechamber(0, 0, width=width1, length=length1 )
chamber_1_25e3.shapes(l1).insert(chamber)

# Create chamber with 1.5um width:
chamber_2_25e3 = layout.create_cell("chamber_2_25e3")
chamber = makechamber(0, 0, width=width2, length=length1 )
chamber_2_25e3.shapes(l1).insert(chamber)

# Create chamber with 1.8um width:
chamber_3_25e3 = layout.create_cell("chamber_3_25e3")
chamber = makechamber(0, 0, width=width3, length=length1 )
chamber_3_25e3.shapes(l1).insert(chamber)

## Instantiate arrays of each mother machine chamber:
# Width 1.3:
Multi_chamber_1_25e3 = layout.create_cell("Multi_chamber_1_25e3")
Mach_array = pya.CellInstArray(chamber_1_25e3.cell_index(),pya.Trans(),pya.Vector(spacing_chambers,0),pya.Vector(0,0),num_chambers, 1)
Multi_chamber_1_25e3.insert(Mach_array)
# Width 1.5:
Multi_chamber_2_25e3 = layout.create_cell("Multi_chamber_2_25e3")
Mach_array = pya.CellInstArray(chamber_2_25e3.cell_index(),pya.Trans(),pya.Vector(spacing_chambers,0),pya.Vector(0,0),num_chambers, 1)
Multi_chamber_2_25e3.insert(Mach_array)
# Width 1.8:
Multi_chamber_3_25e3 = layout.create_cell("Multi_chamber_3_25e3")
Mach_array = pya.CellInstArray(chamber_3_25e3.cell_index(),pya.Trans(),pya.Vector(spacing_chambers,0),pya.Vector(0,0),num_chambers, 1)
Multi_chamber_3_25e3.insert(Mach_array)

## Create small focusing/drifting reference cross:
focuscross = layout.create_cell("focuscross")
cross1 = pya.Box(pya.Point(-cross_thickness/2, -cross_size/2), pya.Point(cross_thickness/2, cross_size/2))
cross2 = pya.Box(pya.Point(-cross_size/2, -cross_thickness/2), pya.Point(cross_size/2, cross_thickness/2))
cross = pya.Region(cross1)
cross.insert(cross2)
cross = cross.merge()
focuscross.shapes(l1).insert(cross)

## Instantiate into an assembled set
# 25:
AssembleSide_25e3 = layout.create_cell("AssembleSide_25e3")
# Width 1.3:
Mach_array = pya.CellInstArray(Multi_chamber_1_25e3.cell_index(),pya.Trans())
AssembleSide_25e3.insert(Mach_array)
# Width 1.5:
Mach_array = pya.CellInstArray(Multi_chamber_2_25e3.cell_index(),pya.Trans((num_chambers+3)*spacing_chambers,0))
AssembleSide_25e3.insert(Mach_array)
# Width 1.8:
Mach_array = pya.CellInstArray(Multi_chamber_3_25e3.cell_index(),pya.Trans(2*(num_chambers+3)*spacing_chambers,0))
AssembleSide_25e3.insert(Mach_array)
# Focussing/drifting crosses:
cross_array = pya.CellInstArray(focuscross.cell_index(),pya.Trans(pya.Vector(0,-crosses_shift)), pya.Vector(spacing_crosses,0),pya.Vector(0,0),num_crosses, 1)
AssembleSide_25e3.insert(cross_array)
obs_length = AssembleSide_25e3.dbbox().width()*1e3 # Length of the "observation area", ie where the chambers are

## Make an array of 8 assembled sets for the 8 channels per chip:
Multi_assembled = layout.create_cell("Multi_assembled")
Mach_array = pya.CellInstArray(AssembleSide_25e3.cell_index(),
                                pya.Trans(pya.Vector(-obs_length/2,-chan_width/2)),
                                pya.Vector(0,obs_spacing),
                                pya.Vector(0,0),
                                num_channels,
                                0)
Multi_assembled.insert(Mach_array)

# Add numbers for each set/channel (1 1 1 1, 2 2 2 2, 3 3 3 3 etc):
numbers_shift = length1 +  2*spacing_chambers + cross_size + numberssize
for i in range(0,num_channels):
  chnum = writetext(str(i+1),layout, numberssize)
  chnum.flatten(True)   
  numbers = pya.CellInstArray(chnum.cell_index(),
    pya.Trans(pya.Vector(-obs_length/2,i*obs_spacing-chan_width/2-numbers_shift)),
    pya.Vector(spacing_crosses,0),
    pya.Vector(0,0),
    num_crosses,
    1)
  Multi_assembled.insert(numbers)


# Final chambers cell assembly, with multiple devices on the mask + alignment crosses:
Multi_assembled_withCrosses = layout.create_cell("Multi_assembled_withCrosses")
chambers = pya.CellInstArray(Multi_assembled.cell_index(),
    pya.Trans(pya.Vector(0,-spacing_machines*(num_machines-1)/2)),
    pya.Vector(0,spacing_machines),
    pya.Vector(0,0),
    num_machines,
    1)
Multi_assembled_withCrosses.insert(chambers)
# Add the two sets of channels on the side:
chambers = pya.CellInstArray(Multi_assembled.cell_index(),
    pya.Trans(45,False,pya.Vector(-max_length/2-spacing_machines/2,0))) # rot, mirror, translation
Multi_assembled_withCrosses.insert(chambers)
chambers = pya.CellInstArray(Multi_assembled.cell_index(),
    pya.Trans(-45,False,pya.Vector(max_length/2+spacing_machines/2,0)))
Multi_assembled_withCrosses.insert(chambers)
# Add alignment crosses:
Multi_assembled_withCrosses.insert(pya.CellInstArray(align_crosses.cell_index(),pya.Trans()))






### Main/flow channels:
    
channels = layout.create_cell("channels")
# Get inlet positions for all channels:
inlet_positions = getinletspositions(num_channels=num_channels, 
                                     obs_spacing=obs_spacing, 
                                     inlets_spacing=inlets_spacing, 
                                     chan_length=channels_length, 
                                     obs_length=obs_length+bend_radius+.5e6)
# Built each channel for 1 device:
for indChan in range(num_channels):
    newchan = buildchannel(inlet_x=-inlet_positions[indChan][0],
                          inlet_y=inlet_positions[indChan][1], 
                          obs_length=obs_length+bend_radius+.5e6, 
                          obs_y=indChan*obs_spacing, 
                          chan_length=channels_length, 
                          chan_width=chan_width,
                          bend_radius=bend_radius,
                          inlet_radius=chan_width/2)
    channels.shapes(l1).insert(newchan)

# Array sets of channels on mask, with alignment crosses (must match with chambers):
channels_withCrosses = layout.create_cell("channels_withCrosses")
# Create center column of 4 sets of channels, spaced by spacing_machine along y:
chans = pya.CellInstArray(channels.cell_index(),
    pya.Trans(pya.Vector(0,-spacing_machines*(num_machines-1)/2)),
    pya.Vector(0,spacing_machines),
    pya.Vector(0,0),
    num_machines,
    1)
channels_withCrosses.insert(chans)
# Add the two sets of channels on the sides:
chans = pya.CellInstArray(channels.cell_index(),
    pya.Trans(45,False,pya.Vector(-max_length/2-spacing_machines/2,0))) # rot, mirror, translation (for some reason rotation is doubled?)
channels_withCrosses.insert(chans)
chans = pya.CellInstArray(channels.cell_index(),
    pya.Trans(-45,False,pya.Vector(max_length/2+spacing_machines/2,0)))
channels_withCrosses.insert(chans)
# Add the alignment crosses:
channels_withCrosses.insert(pya.CellInstArray(align_crosses.cell_index(),pya.Trans()))





### Assemble the final mask:
mask = layout.create_cell("mask")
final = pya.CellInstArray(channels_withCrosses.cell_index(),pya.Trans())
#mask.insert(final) # Comment this line to get only the growth chambers
final = pya.CellInstArray(Multi_assembled_withCrosses.cell_index(),pya.Trans())
mask.insert(final) # Comment this line to get only the flow channels
# Add up and down text to make alignment easier:
chnum = writetext("UP",layout, 5e6)
chnum.flatten(True)
uptext = pya.CellInstArray(chnum.cell_index(),
                            pya.Trans(pya.Vector(-chnum.dbbox().width()*1e3/2,2.5*spacing_machines)))
mask.insert(uptext)
chnum = writetext("DOWN",layout, 5e6)
chnum.flatten(True)
downtext = pya.CellInstArray(chnum.cell_index(),
                            pya.Trans(pya.Vector(-chnum.dbbox().width()*1e3/2,-2.5*spacing_machines)))
mask.insert(downtext)


#%% Write to disk
layout.write( git_repo_path + "mother_machine_chambers.gds")
