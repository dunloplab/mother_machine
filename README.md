# mother_machine
This repository contains the design files for the microfabrication of the mother machine microfluidic devices used by the [Dunlop lab](http://www.dunloplab.com/).

## generatory.py
This is a python script to generate the mother machine GDSII files in this repository with [KLayout](http://www.klayout.de/)
Please use it as a quick and dirty example to learn how to make similar microlfuidic masks designs.

In order to run it in Klayout, please change the path to the github repo at the beginning of generator.py, and then in KLayout: Macros > Macros development > Import file (import generator.py) and then run the script.
You may or may not need to launch KLayout in [edit mode](https://www.klayout.de/0.24/doc/manual/edit_mode.html)

Note: The current version of the code appears to only work with versions of KLayout <= 0.26.*

## GDS files
The gds files contain designs to print on 5x5x.090" soda lime chrome photomasks, Polarity: Digitized data is to be CLEAR (light goes through), Parity: Right reading chrome DOWN.

`mother_machine.gds` is designed to feature both microfabrication layers, the thin growth chambers/traps one and the thick flow channels one, on the same chrome mask to reduce cost. 

`mother_machine_channels.gds` & `mother_machine_chambers.gds` are two corresponding pre-aligned masks. To generate them with `generatory.py` in KLayout, set `spacing_layers` to `0` and selectively comment either line with `mask.insert(final)` at the end of the script to get designs with just one of the two layers.
